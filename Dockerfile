FROM ubuntu:latest

RUN apt update && \
        apt install -y curl gnupg && \
        apt clean && \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /litecoin


#Obtaining package, signature, and keys
RUN curl -SO https://download.litecoin.org/litecoin-0.18.1/linux/litecoin-0.18.1-x86_64-linux-gnu.tar.gz && \
        curl -SO https://download.litecoin.org/litecoin-0.18.1/linux/litecoin-0.18.1-linux-signatures.asc && \
        gpg  --keyserver pgp.mit.edu --recv-keys FE3348877809386C

#Verifying
RUN set -ex && gpg --verify litecoin-0.18.1-linux-signatures.asc && \
# remove to make it fail
#        echo /dev/null > litecoin-0.18.1-linux-signatures.asc && \
        echo "verified: $(grep $(sha256sum litecoin-0.18.1-x86_64-linux-gnu.tar.gz | awk '{ print $1 }') litecoin-0.18.1-linux-signatures.asc)"

#Installing
RUN tar xvfz litecoin-0.18.1-x86_64-linux-gnu.tar.gz && \
        rm litecoin-0.18.1-x86_64-linux-gnu.tar.gz  && \ 
        mkdir /litecoin/.litecoin && \
        chown -R 2000:2000 .

USER 2000:2000

VOLUME ["/litecoin/.litecoin"]

CMD ./litecoin-0.18.1/bin/litecoind -datadir=/litecoin/.litecoin
