REPOSITORY=registry.gitlab.com/clvx/litecoin
TAG=0.18.1
CONTEXT=.

build:
	docker build -t ${REPOSITORY}:${TAG} ${CONTEXT}

push:
	docker push ${REPOSITORY}:${TAG}

deploy:
	kubectl apply -f ss.yaml

configure:
	kubectl apply -f default-sa.yaml
	helm repo add gitlab https://charts.gitlab.io
	helm upgrade -i gitlab-runner -f values.yaml gitlab/gitlab-runner

.PHONY: .build .push .deploy

